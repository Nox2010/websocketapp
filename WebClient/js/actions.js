// Create new table for every connected clients, 4 row for test control buttons
function addNewTable() {
    var table = document.createElement('Table');
    table.setAttribute('class','table table-bordered');
    table.setAttribute('border','1');
    table.setAttribute('width','100%');
    table.setAttribute('id', js_data.username_from);

    var header = table.createTHead();
    var row = header.insertRow(-1);

    var text = document.createTextNode(js_data.username_from);
    var cell = row.insertCell(0);
    cell.setAttribute('align','center');
    cell.setAttribute('width', '25%');
    cell.setAttribute('id', 'name');
    cell.appendChild(text);

    var text = document.createTextNode("alive");
    var cell = row.insertCell(-1);
    cell.setAttribute('align','center');
    cell.setAttribute('width', '10%');
    cell.setAttribute('id', 'status');
    cell.appendChild(text);

    var text = document.createTextNode(js_data.message);
    var cell = row.insertCell(-1);
    cell.setAttribute('align','center');
    cell.setAttribute('width', '50%');
    cell.setAttribute('id', 'message');
    cell.appendChild(text);

    // // Send message to user
    // var button = document.createElement("button");
    // var cell = row.insertCell(-1);
    // button.innerHTML = 'MSG';
    // button.setAttribute("style","color:red");
    // button.setAttribute("id", js_data.username_from)
    // button.addEventListener('click', sendMessage);
    // cell.setAttribute('align','center');
    // cell.appendChild(button);
    // document.getElementById("outputDiv").appendChild(table);

    // Save user to database
    var button = document.createElement("button");
    var cell = row.insertCell(-1);
    button.innerHTML = 'Save to DB';
    button.setAttribute("style","color:red");
    button.setAttribute("id", js_data.username_from)
    button.addEventListener('click', saveClient);
    cell.setAttribute('align','center');
    cell.appendChild(button);
    document.getElementById("outputDiv").appendChild(table);

    // Let's try to kick connected user
    var button = document.createElement("button");
    var cell = row.insertCell(-1);
    button.innerHTML = 'X';
    button.setAttribute("style","color:red");
    button.setAttribute("id", js_data.username_from)
    button.addEventListener('click', removeClient);
    cell.setAttribute('align','center');
    cell.appendChild(button);
    document.getElementById("outputDiv").appendChild(table);
}

function updateTable() {
    tableIdToUpdate = document.getElementById(js_data.username_from);
    if (tableIdToUpdate !== null) {
        //document.message.textContent = js_data.message;
        //document.message.nodeValue = js_data.message;
        //tableIdToUpdate.message.nodeValue = js_data.message;
        //tableIdToUpdate.message.nodeValue = 'test'
        tableIdToUpdate.getElementsByTagName("td")[2].innerHTML = js_data.message;
    } else {
        addNewTable();
    }
}

function saveClient() {
    //alert('pressed save button');
    // TODO: action for manual saving client to db.
    js_data = JSON.stringify({username_from: 'test', message: null, action: 'saveClient'})
    ws.send(js_data)
}

function removeClient() {
    var username = this.getAttribute('id');
    // console.log(username);
    js_data = JSON.stringify({username_from: username, message: username, action: 'kickClient'})
    ws.send(js_data)
    // TODO: action for manual client disconnect.
}

function removeTable() {
    tableIdToRemove = document.getElementById(js_data.username_from);
    tableIdToRemove.parentNode.removeChild(tableIdToRemove);
}


function WebSocketConnect(){
    let div = document.createElement('div');
    div.setAttribute('id', 'outputDiv');
    div.setAttribute('class', 'container');
    document.body.append(div);
    // Connect to websocket server, and retrieve JSON 'status' from php workerman
    ws = new WebSocket("ws://127.0.0.1:2346/?user=Chrome&web=yes");
    ws.onmessage = function(evt) {
        js_data = JSON.parse(evt.data);
        switch(js_data.status){
            case "connected":
                addNewTable();
                break;
            case "disconnected":
                removeTable();
                break;
            case "updated":
                updateTable();
            // case "ping":
            //     updateTable();
        }
    }
}
// WebSocketConnect()

