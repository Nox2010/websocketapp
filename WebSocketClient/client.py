import time
import os
import sys
import config as cfg
import websockets
import asyncio
from urllib.request import urlopen
import logging
import json
import datetime


# Temporary, take filename for simulating multiple connections. (stupid way for test connections, i think)
filename = os.path.splitext(os.path.basename(__file__))[0]
pos_config = {}
start_time = 0.0
BUFFER_SIZE = 1024
logging.basicConfig(filename='client.log', level=logging.INFO, format='%(asctime)s %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S')


def random_json(status='updated'):
    msg_text = ''
    if status == 'updated':
        msg_text = 'updated'
    elif status == 'connected':
        msg_text = 'connected'
    elif status == 'ping':
        life_time = time.time() - start_time
        msg_text = f"{life_time:.2f}"
    else:
        msg_text = 'Unknown msg text'

    data = {
        "username_from": pos_config['egais_address'],
        "username_to": '',
        "status": status,
        "message": msg_text,
        "action": 'MSG2ALL',
    }
    # print(data)
    return data


def script_lifetime():
    time.sleep(1)
    life_time = time.time() - start_time
    print(f'Worked: {life_time:.2f} seconds \n')
    return life_time


# async def receive_json(json_data, ws):
async def receive_json(ws):
    # print(websocket)
    json_data = await ws.recv()
    # data = await websocket.recv()
    # print(data)
    if json_data:
        try:
            deserialized = json.loads(json_data)
            # Just for debug
            # print(deserialized)
            # print(f"< {deserialized['username']}")
            # print(pos_config['egais_address'])
            if deserialized['action'] == 'kicked':
                await ws.close()
                print('bye')
            elif deserialized['action'] == 'status_update':
                print(f"{deserialized['username_from']} - {deserialized['status']}: {deserialized['message']}")

            return deserialized

        except Exception as e:
            print(e)
            logging.error(e)
            script_lifetime()


def parse_line(dictionary, name, line):
    # Parse values after '= ', and append to dictionary
    # values in config file can be empty, so, whe go try.
    try:
        dictionary[name] = line.split('= ')[1].rstrip()
        return dictionary

    except Exception as e:
        logging.error('Cannot parse value from {}'.format(line))
        logging.error(e)
        return None


def utm_parser():
    url = "http://localhost:8080/api/info/list"
    try:
        response = urlopen(url)
        utm_data = {}

        try:
            data_json = json.loads(response.read())
            utm_data['rsa_startDate'] = datetime.datetime.strptime(data_json["rsa"]["startDate"], "%Y-%m-%d %H:%M:%S %z")
            utm_data['rsa_expireDate'] = datetime.datetime.strptime(data_json["rsa"]["expireDate"], "%Y-%m-%d %H:%M:%S %z")
            utm_data['rsa_isValid'] = data_json["rsa"]["isValid"]

            utm_data['gost_startDate'] = datetime.datetime.strptime(data_json["gost"]["startDate"], "%Y-%m-%d %H:%M:%S %z")
            utm_data['gost_expireDate'] = datetime.datetime.strptime(data_json["gost"]["expireDate"], "%Y-%m-%d %H:%M:%S %z")
            utm_data['gost_isValid'] = data_json["gost"]["isValid"]
            return utm_data

        except Exception as e:
            logging.error('UTM parsing error')
            logging.error(e)
            print('UTM parsing error')
            return None
    except Exception as e:
        logging.error('UTM not reachable')
        logging.error(e)
        print('UTM not reachable')
        return None


def get_info_from_file():
    # Read config file, and get values.
    # TODO use %appdata% to shorten path and test on several winOS

    config_path = r'C:\Users\User\AppData\Local\VirtualStore\Program Files (x86)\ШТРИХ-М\Кассир мини\DB\common.txt'
    config_data = {}
    fr_region = False
    egais_region = False

    logging.info('Start read data from {}'.format(config_path))
    if os.path.exists(config_path):
        with open(config_path) as file:
            data = file.readlines()
            for line in data:
                if "<ФР>" in line:
                    fr_region = True
                elif "</ФР>" in line:
                    fr_region = False
                elif "<ЕГАИС>" in line:
                    egais_region = True
                elif "</ЕГАИС>" in line:
                    egais_region = False

                if "Версия " in line:
                    parse_line(config_data, 'soft_version', line)

                if fr_region and "Ком-порт " in line:
                    parse_line(config_data, 'fiscal_com_port', line)

                if fr_region and "Хост " in line:
                    parse_line(config_data, 'fiscal_ip_address', line)

                if egais_region and "НазваниеОрганизации0 " in line:
                    parse_line(config_data, 'egais_ooo', line)

                if egais_region and "ИНН0 " in line:
                    parse_line(config_data, 'egais_inn', line)

                if egais_region and "КПП0 " in line:
                    parse_line(config_data, 'egais_kpp', line)

                if egais_region and "Адрес0 " in line:
                    parse_line(config_data, 'egais_address', line)

        logging.info('File read ok, data parsed ')
        return config_data
    else:
        config_data = {}
        config_data['egais_address'] = filename
        logging.error('File {} not found!'.format(config_path))
        return config_data


async def send_json(websocket, status='updated', timeout=5):
    # send json is send json. I think it for send json. god bless json.
    try:
        await asyncio.wait_for(websocket.send(json.dumps(random_json(status))), timeout)

    except Exception as e:
        logging.error("Message sending error")
        print("Message sending error")
        logging.error(e)
        # await websocket.close()
        script_lifetime()


async def websocket_connect(user_name):
    # Try connect to WebSockServer, after connection send json with 'connected' 'status'
    # It need for Web Control Panel, for index.html → case 'connected' → action.js → addNewTable()
    # uri = f"ws://localhost:2346/?user={user_name}&remote=yes"
    uri = f"ws://{cfg.server_ip}:{cfg.server_port}/?user={user_name}&remote=yes"
    websocket = ''

    while not websocket:
        try:
            websocket = await asyncio.wait_for(websockets.connect(uri, ping_interval=None), cfg.connection_timeout)
            try:
                await send_json(websocket, "connected")
                print('connected')
            except Exception as e:
                logging.error('Sending message after connection error')
                print("Sending message after connection error")
                logging.error(e)
                script_lifetime()
            return websocket

        except Exception as e:
            logging.error('Connection error:')
            print("Connection error")
            logging.error(e)
            script_lifetime()
            logging.info('Sleep {} sec and try connect'.format(cfg.connection_timeout))
            time.sleep(cfg.connection_timeout)


async def main(user_name):
    while True:
        # Connect to WS, and try to keep alive.
        # Reconnecting test ok. If server die - client keep wait for them, without crash.
        global start_time
        websocket = await websocket_connect(user_name)
        start_time = time.time()
        # Continuously send json data with random numeric message. And receive this on the Web Panel
        while True:
            if not websocket.closed:
                try:
                    # Try receive JSON and deserialize them.
                    recv_task = asyncio.create_task(receive_json(websocket))
                    ping_spam = asyncio.create_task(send_json(websocket, 'ping'))
                    # json_data = await websocket.recv()
                    # await receive_json(websocket)
                    # await send_json(websocket, 'ping')
                    await asyncio.sleep(1)

                except websockets.ConnectionClosed:
                    print(f"Connection closed. Reconnecting?..")
                    script_lifetime()
                    break
            else:
                try:
                    recv_task.cancel()
                    ping_spam.cancel()
                    await asyncio.sleep(5)
                except Exception as e:
                    print(e)
                break


if __name__ == '__main__':
    pos_config = get_info_from_file()
    # TODO generate random name if file not exist, or incorrect data
    utm_config = utm_parser()
    asyncio.get_event_loop().run_until_complete(main(pos_config['egais_address']))
    asyncio.get_event_loop().run_forever()


