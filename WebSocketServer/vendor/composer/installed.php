<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'd4589cd2894fd7b88a2428e8a04995fd1b0ed0c4',
        'name' => 'nox2010/websocketserver',
        'dev' => true,
    ),
    'versions' => array(
        'nox2010/websocketserver' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'd4589cd2894fd7b88a2428e8a04995fd1b0ed0c4',
            'dev_requirement' => false,
        ),
        'workerman/workerman' => array(
            'pretty_version' => 'v4.0.19',
            'version' => '4.0.19.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/workerman',
            'aliases' => array(),
            'reference' => 'af6025976fba817eeb4d5fbf8d0c1059a5819da3',
            'dev_requirement' => false,
        ),
    ),
);
