<?php

use Workerman\Worker;

require_once __DIR__ . '/vendor/autoload.php';

$users = [];
$saved_users = [];

// Create a Websocket server
$ws_worker = new Worker('websocket://0.0.0.0:2346');

// 4 processes
$ws_worker->count = 4;

// Emitted when new connection come
$ws_worker->onConnect = function ($connection) use (&$users){
    $connection->onWebSocketConnect = function ($connection) use (&$users)
    {
        $msg = '';
        // check 'remote' tag, need to detect device-clients
        if(isset($_GET['remote']) and $_GET['remote'] == "yes") {
            $connection->remote = true;
            $connection->web = false;
            $msg = ' / is Remote';
        } elseif (isset($_GET['web']) and $_GET['web'] == "yes"){
            $connection->web = true;
            $connection->remote = false;
            $msg = ' / is Web';
        }

        // Add name from GET request
        if(isset($_GET['user'])){
            $connection->username = $_GET['user'];
        }

        echo "New connection\n";
        echo $connection->username  . $msg . "\n"
            . $connection->getRemoteAddress(). "\n\n";

        // Add connected user to array $users
        $users[$_GET['user']] = $connection;
        //print_r(array_keys($users));


        // Send JSON to each connected clients with 'remote' tag in $array, just for test, i think.
//        foreach ($users as $item) {
//            if ($item->remote == true or $item->web == true) {
//                $json_arr = array(
//                    'username_from' => $_GET['user'],
//                    'username_to' => '',
//                    'status' => 'connected',
//                    'message' => 'waiting for response',
//                    'action' => 'status_update',
//                );
//
//                $item->send(json_encode($json_arr));
//                unset($item);
//                unset($json_arr);
//            } else {
//                echo $item->username." : Remote or Web = false\n";
//            }
//        }
    };
};

// Emitted when data received
$ws_worker->onMessage = function ($connection, $data) use (&$users) {
    // After receiving message from client - we send the another useless JSON.
    //$json_arr = [];
    $data = json_decode($data);

    if ($data != null){
        $action = $data->action;
        switch ($action){
            case "MSG2ALL":
                $message_text = $data->message;
                foreach ($users as $item)  {
                    if ($item->web == true) {
                        $user = array_search($connection, $users);
                        $json_arr = array(
                            'username_from' => $user,
                            'username_to' => '',
                            'status' => 'updated',
                            'message' => $message_text,
                            'action' => 'status_update',
                        );

                        $item -> send(json_encode($json_arr));
                        unset($item);
                        unset($json_arr);
                        unset($user);

                    } else {
                        unset($item);
                    }
                    //unset($message);
                }
                unset($data);
                unset($item);
                break;

//            case "MSG2WEB":
//                $message_text = $data->message;
//                foreach ($users as $item)  {
//                    if ($item->web == true) {
//                        $user = array_search($connection, $users);
//                        $json_arr = array(
//                            'username_from' => $user,
//                            'username_to' => '',
//                            'status' => 'updated',
//                            'message' => $message_text,
//                            'action' => 'status_update',
//                        );
//
//                        $item -> send(json_encode($json_arr));
//                        unset($item);
//                        unset($json_arr);
//                        unset($user);
//
//                    } else {
//                        unset($item);
//                    }
//                    //unset($message);
//                }
//                unset($data);
//                unset($item);
//                break;

            case "saveClient":
                echo $connection->username . " press save button\n";
                unset($data);
                break;

            case "kickClient":
                echo $connection->username . " press kick button for → " . $data->message ."\n";
//                $kickuser = $users[$data->message];
                $json_arr = array(
                            'username_from' => $connection->username,
                            'username_to' => $data->message,
                            'status' => 'updated',
                            'message' => 'go away',
                            'action' => 'kicked',
                        );
                $users[$data->message] -> send(json_encode($json_arr));
                unset($data);
                break;
        }
    }
};

// Emitted when connection closed
$ws_worker->onClose = function ($connection) use (&$users){
    // Remove disconnected user from array
    $disconnectedUser = array_search($connection, $users);
    unset($users[$disconnectedUser]);
    echo $disconnectedUser." - Connection closed\n";
    //print_r(array_keys($users));

    // Send JSON to each connected clients about disconnected user. Useless right now, but will remain for testing.
    foreach ($users as $item) {
        if ($item->remote == true or $item->web == true) {
            $json_arr = array(
                'username_from' => $disconnectedUser,
                'username_to' => '',
                'status' => 'disconnected',
                'message' => $disconnectedUser . " has been disconnected",
                'action' => 'status_update',
            );

            $item->send(json_encode($json_arr));
            unset($item);
            unset($json_arr);
        } else {
            unset($item);
        }
    }

};

// Run worker
Worker::runAll();